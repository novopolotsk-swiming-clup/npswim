# NpSwim

#### запуск frontend части 
`yarn run dev`

#### запуск backend части 
`php bin/console server:run`

#### установить миграции
`php bin/console doctrine:migrations:migrate`

#### загрузить фикстуры
`php bin/console doctrine:fixtures:load`