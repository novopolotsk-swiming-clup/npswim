<?php

namespace App\DataFixtures;
use App\Entity\User;
use App\Entity\Role;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $role = $this->loadRoles($manager);
        $this->loadUsers($manager, $role);
        $manager->flush();
    }

    public function loadRoles($manager)
    {
        $roleAdmin = new Role();
        $roleAdmin->setRoleName('ROLE_ADMIN');
        $manager->persist($roleAdmin);

        $roleUser = new Role();
        $roleUser->setRoleName('ROLE_USER');
        $manager->persist($roleUser);
        return $roleAdmin;
    }

    public function loadUsers($manager, $role)
    {
        $user = new User();
        $user->setUsername('thwacka');
        $user->setPassword('BFEQkknI/c+Nd7BaG7AaiyTfUFby/pkMHy3UsYqKqDcmvHoPRX/ame9TnVuOV2GrBH0JK9g4koW+CgTYI9mK+w==');
        $user->setFirstname('Test');
        $user->setSecondname('User');
        $user->setEmail('admin@test.local');
        $user->setRoles($role);

        $manager->persist($user);
    }
}
