<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

use \Datetime;
use App\Entity\User;

class UserController extends AbstractController
{
    /**
     * @Route("/api/login", methods={"POST"}, name="user_login")
     */
    public function login(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $encodedPassword = $encoder->encodePassword(new User(), $request->get('password'));

        $user = $entityManager->getRepository(User::class)->findOneBy([
            'email' => $request->get('email'),
            'password' => $encodedPassword
        ]);
        if (!$user) {
            throw $this->createAccessDeniedException();
        }
        
        return $this->json(['success' => true]);
    }
}
