import * as React from 'react';
import Login from './pages/login';
import './style/app.scss';
import * as ReactDOM from 'react-dom';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import { Provider } from 'react-redux';

class App extends React.Component {
  render(): React.ReactNode {
    return (
      <Login />
    )
  }
}

export default App;
/*
      <Provider store={store}>
        <BrowserRouter>
          <Switch>
            <Route path={"/login"} render={(props) => (!this.state.user.isLogged ? <SingInContainer {...props} /> : <Redirect to={"/app"} />)} />
          </Switch>
        </BrowserRouter>
      </Provider>
*/