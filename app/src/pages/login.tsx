import * as React from 'react';
import Input from '../components/inputs/input';
import PrimaryButton from '../components/buttons/primaryButton';
import '../style/pages/login.scss';

interface State {
	email: string,
	password: string,
}

class Login extends React.Component<{}, State> {

	state: State = {
		email: '',
		password: ''
	}

	render(): React.ReactNode {
		return (
			<div className='container'>
				<form action="" className='panel'>
					<Input
						type={'email'}
						value={this.state.email}
						placeholder={'Email address'}
					/>

					<Input
						type={'password'}
						value={this.state.password}
						placeholder={'Password'}
					/>

					<PrimaryButton 
						label={'Log in'}
					/>
				</form>
			</div>
		)
	}
}

export default Login;