import * as React from 'react';
import '../../style/components/input.scss';

interface Props {
	type: string,
	value: string,
	placeholder: string
}

class Input extends React.Component<Props> {
	render(): React.ReactNode {
		return (
			<input
				className='input'
				type={this.props.type}
				value={this.props.value}
				placeholder={this.props.placeholder}
			/>
		)
	}
}

export default Input;