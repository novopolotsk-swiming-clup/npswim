import * as React from 'react';
import '../../style/components/button.scss';

interface Props {
	label: string,
}

class PrimaryButton extends React.Component<Props> {
	render(): React.ReactNode {
		return (
			<input
				className='primary-btn'
				type={'button'}
				value={this.props.label}
			/>
		)
	}
}

export default PrimaryButton;