import axios from 'axios';
import { AxiosPromise } from 'axios';

export interface IUser {
    id: number,
    firstname: string,
    secondname: string,
    email: string,
    accessToken: string,
    refreshToken: string,
}

export function authUser(email: string, password: string): AxiosPromise<IUser> {
    return axios.post('/api/login', {email, password});
}   