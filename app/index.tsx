
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from './src/app';
import './src/style/app.scss';

ReactDOM.render(
    <App/>,
    document.getElementById('root')
);