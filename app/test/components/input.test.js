import * as React from 'react';
import Input from '../../src/components/inputs/input';
import renderer from 'react-test-renderer';
import { shallow } from 'enzyme';

describe('<Input />', () => {
	it('Correctly render input component', () => {
		const inputComponent = shallow(React.createClass(<Input />));
		expect(inputComponent).toHaveLength(1);
	})
});